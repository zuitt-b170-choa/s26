 let http = require("http"); 
 const port = 4000;
 const server = http.createServer((request, response)=>{
 	// selection control structures - it selects the function depending on the request
 	if(request.url === "/greeting"){
 		response.writeHead(200, {"Content-Type": "text/plain"});
 		response.end("Hello World");
            }
 		/*
			Assignment/Practice 
				create two more uri's and let your users see the message "Welcome to _____ Page"; use successful status code and plain text as the content.
 		*/
    else if(request.url === "/home"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Welcome to Home Page");
    }
    else if(request.url === "/contact"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Welcome to Contact Page");
 	}
 	else{
 		response.writeHead(404, {"Content-Type": "text/plain"});
 		response.end("Page not found.");
 	}

 });

 server.listen(port);

console.log(`Server now running at localhost: ${port}`);