let http = require("http"); 
/*
	require function is a directive/function that is used to load a particular node module.
		http module - HyperText Transfer Protocol; it lets node.js transfer data; a set of individual files that are needed to create a component (used to establish data transfer between applications)
*/

http.createServer(function(request, response){
	/*
		createServer() - allows creation of http server that listens to requests on a specified port and gives response back to the client.
	*/
	response.writeHead(200, {"Content-Type": "text/plain"});
	response.end("Hello World");
}).listen(4000);

console.log("Your server is running at port: 4000");

/*
	response.writeHead() - used to set a status code for the response: 200 status code is the default code for the node.js since this means that the response is successfully processed.
	https://developer.mozilla.org/en-US/docs/Web/HTTP/Status - reference link
	
	reponse.end() - this signals the end of the response

	listen() - the server will be assigned to the specified port using this command
	port - the virtual point where the network connections start and end; each port is specific to a certain process/server

	Content-Type - sets the type of the content which will be displayed on the client side.
	to test: search in browser: portquiz.net:<value>

	ctrl + c - the gitbash will terminate any process being done in the directory/local repo
*/